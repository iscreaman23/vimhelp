# vimhelp

A vim plugin to enhance the functionality of :help.

## Features
Adds additional mapping for help buffer:
* tab - jump to next link
* shift tab - previous link
* Enter - Follow link (if highlighting one)
* K - jump to next jump location in history
* J - jump to previous jump location in history

