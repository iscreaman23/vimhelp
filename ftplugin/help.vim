if exists("b:did_ftplugin")
	finish
endif

setlocal nohlsearch

let b:did_ftplugin = 1

nnoremap <buffer><silent>(vimhelp-next-link)         		:call help#TabJump(0)<CR>
nnoremap <buffer><silent>(vimhelp-previous-link)            :call help#TabJump(1)<CR>
nnoremap <buffer><silent>(vimhelp-follow-link)            	<C-]>
nnoremap <buffer><silent>(vimhelp-forward)            		<C-i>
nnoremap <buffer><silent>(vimhelp-back)            			<C-o>
nnoremap <buffer><silent>(vimhelp-quit)            			:q<CR>

if !exists('g:vimhelp#disable_default_keymap') || g:vimhelp#disable_default_keymap == 0
	nmap <buffer><Tab> 		(vimhelp-next-link)
	nmap <buffer><S-Tab> 	(vimhelp-previous-link)
	nmap <buffer><CR> 		(vimhelp-follow-link)
	nmap <buffer>K 			(vimhelp-forward)
	nmap <buffer>J 			(vimhelp-back)
	nmap <buffer>q 			(vimhelp-quit)
endif

func! help#TabJump(mode)
	let @/ = '|.\{-}|'

	if a:mode==0
		call feedkeys("n",'n')
		call feedkeys("l",'n')
	else
		call feedkeys("NN",'n')
		call feedkeys("l",'n')
	endif

endfunc
